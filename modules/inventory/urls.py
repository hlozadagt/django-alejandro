from modules.inventory.routes import router
from django.urls import include, path


urlpatterns = [
    path('', include(router.urls)),
]