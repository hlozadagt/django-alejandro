from django.db import models


class Products(models.Model):
    product_name = models.CharField(
        verbose_name='Nombre del producto',
        max_length=128
    )

    def __str__(self):
        return '%s' % (self.product_name)

    def __repr__(self):
        return '<Products: %s>' % (self.product_name)

    class Meta:
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'