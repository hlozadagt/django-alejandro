from modules.inventory.models import Products
from modules.inventory.serializers import ProductsSerializer
from rest_framework import viewsets


class ProductsViewSet(viewsets.ModelViewSet):
    queryset = Products.objects.all()
    serializer_class = ProductsSerializer
