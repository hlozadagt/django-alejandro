from modules.inventory.views import ProductsViewSet
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'products', ProductsViewSet)
